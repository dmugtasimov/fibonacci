from functools import wraps
import math

def f(n):
    if n < 2:
        return n
    return f(n - 1) + f(n - 2)

def memorize(func):
    @wraps(func)
    def g(n, memory={0: 0, 1: 1}):
        r = memory.get(n)
        if r is None:
            r = func(n)
            memory[n] = r
        return r
    return g

@memorize
def decorated_f(n):
    return decorated_f(n - 1) + decorated_f(n - 2)

resetable_memory = {0: 0, 1: 1}
def memorize_resetable(func):
    @wraps(func)
    def g(n, reset=False):
        global resetable_memory
        if reset:
            resetable_memory = {0: 0, 1: 1}
        r = resetable_memory.get(n)
        if r is None:
            r = func(n)
            resetable_memory[n] = r
        return r
    return g

@memorize_resetable
def decorated_f_resetable(n):
    return decorated_f_resetable(n - 1) + decorated_f_resetable(n - 2)

def builtin_list_memoization_f(n, memory=[0, 1]):
    if n < len(memory):
        return memory[n]
    else:
        r = builtin_list_memoization_f(n - 1) + builtin_list_memoization_f(n - 2)
        memory.append(r)
        return r

resetable_memory_list = [0, 1]
def builtin_list_memoization_f_resetable(n, reset=False):
    global resetable_memory_list
    if reset:
        resetable_memory_list = [0, 1]
    if n < len(resetable_memory_list):
        return resetable_memory_list[n]
    else:
        r = (builtin_list_memoization_f_resetable(n - 1) +
             builtin_list_memoization_f(n - 2))
        resetable_memory_list.append(r)
        return r

def loop_f(n):
    if n < 2:
        return n
    pp = 0
    p = 1
    for i in xrange(n - 1):
        pp, p = p, pp + p
    return p

@memorize
def loop_f_decorated(n):
    return loop_f(n)

def one_liner_f(n):
    return n if n < 1 else reduce(lambda x, y: x + (x[-1] + x[-2],), xrange(1, n), (0, 1))[-1]

SQRT_5 = math.sqrt(5)
GOLDEN_RATION = (1 + SQRT_5) / 2
OTHER_PRECALC = (1 - SQRT_5) / 2

ONE_PLUS_SQRT_5 = (1 + SQRT_5)
ONE_MINUS_SQRT_5 = (1 - SQRT_5)

def closed_form_f(n):
    return round((GOLDEN_RATION**n - OTHER_PRECALC**n) / SQRT_5)

def closed_form_f_improved(n):
    return round((ONE_PLUS_SQRT_5**n - ONE_MINUS_SQRT_5**n) / (2**n * SQRT_5))

def closed_form_f_more_improved(n):
    return round(GOLDEN_RATION**n / SQRT_5)
