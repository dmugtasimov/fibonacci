import unittest
import time
import gc
from fibonacci import (f, decorated_f, builtin_list_memoization_f,
                       decorated_f_resetable,
                       builtin_list_memoization_f_resetable,
                       loop_f, loop_f_decorated, one_liner_f, closed_form_f,
                       closed_form_f_improved, closed_form_f_more_improved)

class FibonacciTest(unittest.TestCase):
    def _test_time_taken(self, my_f, n=32):
        gc.disable()
        start = time.time()
        my_f(n)
        finish = time.time()
        gc.enable()
        print 'Time taken for %s(%s): %.6f seconds' % (my_f.__name__, n,
                                                       (finish - start))

    def _test_closed_form(self, my_f):
        rounding_error = False
        overflow_error = False
        n = 0
        while not (rounding_error and overflow_error):
            try:
                self.assertEquals(loop_f_decorated(n), my_f(n))
            except AssertionError:
                if not rounding_error:
                    print 'Rounding error at %s(%s)' % (my_f.__name__, n)
                    rounding_error = True
            except OverflowError:
                if not overflow_error:
                    print 'Numeric overflow error at %s(%s)' % (my_f.__name__, n)
                    overflow_error = True
            n += 1

    def _test_recursion_depth(self, my_f):
        n = 0
        while 1:
            try:
                self.assertEquals(my_f(n, reset=True), loop_f_decorated(n))
            except RuntimeError as e:
                if e.message == 'maximum recursion depth exceeded':
                    print 'Stack overflow error at %s(%s)' % (my_f.__name__, n)
                    break
                else:
                    raise
            n += 1

    def _test_functional(self, my_f):
        self.assertEquals(0, my_f(0))
        self.assertEquals(1, my_f(1))
        self.assertEquals(1, my_f(2))
        self.assertEquals(2, my_f(3))
        for i in xrange(4, 17):
            self.assertEquals(my_f(i - 1) + my_f(i - 2), my_f(i))

    def test_original_f(self):
        print
        self._test_time_taken(f)
        self._test_functional(f)

    def test_decorated_f(self):
        print
        self._test_time_taken(decorated_f)
        self._test_functional(decorated_f)

    def test_builtin_list_memoization_f(self):
        print
        self._test_time_taken(builtin_list_memoization_f)
        self._test_functional(builtin_list_memoization_f)

    def test_loop_f(self):
        print
        self._test_time_taken(loop_f)
        self._test_time_taken(loop_f, 70)
        self._test_time_taken(loop_f, 100)
        self._test_time_taken(loop_f, 1000)
        self._test_time_taken(loop_f, 10000)
        self._test_time_taken(loop_f, 100000)
        self._test_time_taken(loop_f, 1000000)
        self._test_functional(loop_f)

    def test_loop_f_decorated(self):
        print
        self._test_time_taken(loop_f_decorated)
        self._test_time_taken(loop_f_decorated, 10000)
        self._test_functional(loop_f_decorated)

    def test_one_liner_f(self):
        print
        self._test_time_taken(one_liner_f)
        self._test_time_taken(one_liner_f, 70)
        self._test_time_taken(one_liner_f, 10000)
        self._test_functional(one_liner_f)

    def test_closed_form_f(self):
        print
        self._test_time_taken(closed_form_f)
        self._test_time_taken(closed_form_f, 70)
        self._test_functional(closed_form_f)

    def test_closed_form_f_improved(self):
        print
        self._test_time_taken(closed_form_f_improved)
        self._test_time_taken(closed_form_f_improved, 70)
        self._test_functional(closed_form_f_improved)

    def test_closed_form_f_more_improved(self):
        print
        self._test_time_taken(closed_form_f_more_improved)
        self._test_time_taken(closed_form_f_more_improved, 70)
        self._test_functional(closed_form_f_more_improved)

    def test_closed_form_f_large_values(self):
        print
        self._test_closed_form(closed_form_f)
        self._test_closed_form(closed_form_f_improved)
        self._test_closed_form(closed_form_f_more_improved)

    def test_recursion_depth(self):
        print
        self._test_recursion_depth(decorated_f_resetable)
        self._test_recursion_depth(builtin_list_memoization_f_resetable)
